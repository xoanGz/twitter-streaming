# Twitter Streaming RestAPI for Kafka 0.10.2.0 API
This project has used as reference this github:
https://github.com/confluentinc/examples/tree/3.2.x/kafka-streams

## Compile the jar and Build the image
```
$ mvn clean package docker:build
...
```


## Environment Variables:
```
app.id = Id of the application (by default twitter-streaming)
bootstrap.servers = Kafka bootstrap server (broker to talk to). Must by configured
state.dir = directory of the temporal files for rockdb (by default tmp)
host.name = name of the host for the rest proxy
host.port = port for the rest proxy
time-window-ms = milisecond of the windows for sentiments mean
```