package com.kafka.twitter.streaming;

import java.util.ArrayList;
import java.util.List;
import java.util.Properties;
import java.util.concurrent.ExecutionException;

import org.apache.kafka.clients.consumer.ConsumerConfig;
import org.apache.kafka.clients.producer.ProducerConfig;
import org.apache.kafka.common.serialization.Serdes;
import org.apache.kafka.streams.StreamsConfig;
import org.apache.kafka.test.TestUtils;
import org.codehaus.jackson.map.ObjectMapper;
import org.junit.BeforeClass;
import org.junit.ClassRule;

import com.kafka.twitter.streaming.kafka.EmbeddedSingleNodeKafkaCluster;

/**
 * Created by xoan on 18/07/17.
 */
public abstract class TwitterStreamingTestAbs {

	/** The list of topics*/
	protected static final List<String> topicList = new ArrayList<String>() {
		private static final long serialVersionUID = 1L;
		{
			add("inputTopic");
			add("outputTopic");
		}
	};
	/** The stream configuration */
	protected static Properties streamsConfiguration = new Properties();
	/** Instance of the class to test */
	protected final TwitterStreamingImpl twitterStreaming = new TwitterStreamingImpl();
	/** Object mapper for json  */
	protected final ObjectMapper mapper = new ObjectMapper();

	@ClassRule
	public static final EmbeddedSingleNodeKafkaCluster CLUSTER = new EmbeddedSingleNodeKafkaCluster();

	@BeforeClass
	public static void setup() throws Exception {
		
		/** Create the topics*/
		topicList.stream().forEach(CLUSTER::createTopic);

		// Configure and start the processor topology.
		streamsConfiguration.put(StreamsConfig.APPLICATION_ID_CONFIG, "twitter-streaming-test");
		streamsConfiguration.put(StreamsConfig.BOOTSTRAP_SERVERS_CONFIG, CLUSTER.bootstrapServers());
		streamsConfiguration.put(StreamsConfig.KEY_SERDE_CLASS_CONFIG, Serdes.Integer().getClass().getName());
		streamsConfiguration.put(StreamsConfig.VALUE_SERDE_CLASS_CONFIG, Serdes.Integer().getClass().getName());
		// The commit interval for flushing records to state stores and
		// downstream must be lower than
		// this integration test's timeout (30 secs) to ensure we observe the
		// expected processing results.
		streamsConfiguration.put(StreamsConfig.COMMIT_INTERVAL_MS_CONFIG, 10 * 1000);
		streamsConfiguration.put(ConsumerConfig.AUTO_OFFSET_RESET_CONFIG, "earliest");
		// Use a temporary directory for storing state, which will be
		// automatically removed after the test.
		streamsConfiguration.put(StreamsConfig.STATE_DIR_CONFIG, TestUtils.tempDirectory().getAbsolutePath());

	}

	/**
	 * Get Producer Configuration
	 * 
	 * @throws ExecutionException
	 * @throws InterruptedException
	 */
	protected Properties getProducerConfig() {
		Properties producerConfig = new Properties();
		producerConfig.put(ProducerConfig.BOOTSTRAP_SERVERS_CONFIG, CLUSTER.bootstrapServers());
		producerConfig.put(ProducerConfig.ACKS_CONFIG, "all");
		producerConfig.put(ProducerConfig.RETRIES_CONFIG, 0);
		return producerConfig;
	}

	/**
	 * Get Consumer Configuration
	 * 
	 * @throws ExecutionException
	 * @throws InterruptedException
	 */
	protected Properties getConsumerConfig() {
		Properties consumerConfig = new Properties();
		consumerConfig.put(ConsumerConfig.BOOTSTRAP_SERVERS_CONFIG, CLUSTER.bootstrapServers());
		consumerConfig.put(ConsumerConfig.GROUP_ID_CONFIG, "twitter-streaming-test-standard-consumer");
		consumerConfig.put(ConsumerConfig.AUTO_OFFSET_RESET_CONFIG, "earliest");
		return consumerConfig;
	}
}
