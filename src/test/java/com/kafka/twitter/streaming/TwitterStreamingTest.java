package com.kafka.twitter.streaming;

import static org.assertj.core.api.Assertions.assertThat;
import static org.mockito.ArgumentMatchers.any;
import static org.mockito.Mockito.mock;
import static org.mockito.Mockito.when;

import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Properties;

import org.apache.kafka.clients.consumer.ConsumerConfig;
import org.apache.kafka.clients.producer.ProducerConfig;
import org.apache.kafka.common.serialization.IntegerDeserializer;
import org.apache.kafka.common.serialization.IntegerSerializer;
import org.apache.kafka.common.serialization.Serdes;
import org.apache.kafka.common.serialization.StringDeserializer;
import org.apache.kafka.common.serialization.StringSerializer;
import org.apache.kafka.streams.KafkaStreams;
import org.apache.kafka.streams.kstream.KStream;
import org.apache.kafka.streams.kstream.KStreamBuilder;
import org.apache.kafka.streams.kstream.TimeWindows;
import org.apache.kafka.streams.state.QueryableStoreTypes;
import org.apache.kafka.streams.state.ReadOnlyKeyValueStore;
import org.apache.kafka.streams.state.ReadOnlyWindowStore;
import org.junit.Test;

import com.kafka.twitter.streaming.dao.SentimentDAO;
import com.kafka.twitter.streaming.model.HashTagSentiment;
import com.kafka.twitter.streaming.model.agg.AvgValue;
import com.kafka.twitter.streaming.model.tweet.TweetVO;
import com.kafka.twitter.streaming.twitter.TwitterTestUtils;
import com.kafka.twitter.streaming.util.SerdesUtil;

/**
 * Created by davidmartinez on 30/06/17.
 */
public class TwitterStreamingTest extends TwitterStreamingTestAbs {

	@Test
	public void generateTweetSentimentStreamTest() throws Exception {

		// Access to tweet topic for test the creation of the sentiment stream
		final KStreamBuilder kStreamBuilder = new KStreamBuilder();
		KStream<String, TweetVO> tweetsStream = kStreamBuilder.stream(Serdes.String(),
				SerdesUtil.createSerde(TweetVO.class), topicList.get(0));

		// Mock GetSentiment DAO
		final SentimentDAO sentimentDAO = mock(SentimentDAO.class);
		when(sentimentDAO.getSentiment(any(), any())).thenReturn(5);

		// Create stream with sentiment info
		final KStream<String, Integer> sentimentStream = twitterStreaming.generateTweetSentimentStream(tweetsStream,
				sentimentDAO);

		// Sink to output topic
		sentimentStream.to(Serdes.String(), Serdes.Integer(), topicList.get(1));

		// Start the kafka client
		KafkaStreams streams = new KafkaStreams(kStreamBuilder, streamsConfiguration);
		streams.start();

		// Produce some input data to the input topic.
		Properties producerProperties = super.getProducerConfig();
		producerProperties.put(ProducerConfig.KEY_SERIALIZER_CLASS_CONFIG, StringSerializer.class);
		producerProperties.put(ProducerConfig.VALUE_SERIALIZER_CLASS_CONFIG, StringSerializer.class);
		IntegrationTestUtils.produceKeyValuesSynchronously(topicList.get(0),
				TwitterTestUtils.generateHashtagsForProducer(), producerProperties);

		// Consumer generated sentiment stream.
		Properties consumerProperties = super.getConsumerConfig();
		consumerProperties.put(ConsumerConfig.KEY_DESERIALIZER_CLASS_CONFIG, StringDeserializer.class);
		consumerProperties.put(ConsumerConfig.VALUE_DESERIALIZER_CLASS_CONFIG, IntegerDeserializer.class);
		List<Integer> actualValues = IntegrationTestUtils.waitUntilMinValuesRecordsReceived(consumerProperties,
				topicList.get(1), 1);
		streams.close();

		// Check de data in the new topic
		assertThat(actualValues.get(0)).isEqualTo(5);

	}

	@Test
	public void createAggEMAStreamTest() throws Exception {

		KafkaStreams streams = null;
		try {
			// Access to sentiment topic for test the creation of the stream
			final KStreamBuilder kStreamBuilder = new KStreamBuilder();
			KStream<String, Integer> sentimentStream = kStreamBuilder.stream(Serdes.String(), Serdes.Integer(),
					topicList.get(0));

			// Create the ema sentiment stream
			twitterStreaming.createAggEMAStream(sentimentStream, "ema-store");

			// Start the kafka client
			streams = new KafkaStreams(kStreamBuilder, streamsConfiguration);
			streams.start();

			// Produce some input data to the input topic.
			Properties producerProperties = super.getProducerConfig();
			producerProperties.put(ProducerConfig.KEY_SERIALIZER_CLASS_CONFIG, StringSerializer.class);
			producerProperties.put(ProducerConfig.VALUE_SERIALIZER_CLASS_CONFIG, IntegerSerializer.class);
			IntegrationTestUtils.produceKeyValuesSynchronously(topicList.get(0),
					TwitterTestUtils.generateTweetSentimentsForProducer(), producerProperties);

			ReadOnlyKeyValueStore<String, HashTagSentiment> store = IntegrationTestUtils.waitUntilStoreIsQueryable(
					"ema-store", QueryableStoreTypes.<String, HashTagSentiment>keyValueStore(), streams);

			// Calculate de the emea
			Double expectedEmea = TwitterTestUtils.emeaOfSentiments();
			// wait one sec for all the data has been injected by the kafka
			// stream
			Thread.sleep(1000L);
			// Get the value from the store and Check de data in the new topic
			final HashTagSentiment value = store.get("#test1");
			assertThat(value.getEmea()).isEqualTo(expectedEmea);
		} finally {
			if (streams != null) {
				streams.close();
			}
		}
	}

	@Test
	public void createDailyAvgTest() throws Exception {
		KafkaStreams streams = null;
		try {
			// Access to sentiment topic for test the creation of the stream
			final KStreamBuilder kStreamBuilder = new KStreamBuilder();
			KStream<String, Integer> sentimentStream = kStreamBuilder.stream(Serdes.String(), Serdes.Integer(),
					topicList.get(0));

			// create daily average stream
			TimeWindows twoSecWindows = TimeWindows.of(2000L).until(2000L);
			twitterStreaming.createDailyAvg(sentimentStream, twoSecWindows, "avg-store", topicList.get(1));

			// Start the kafka client
			streams = new KafkaStreams(kStreamBuilder, streamsConfiguration);
			streams.start();

			// Produce some input data to the input topic.
			Properties producerProperties = super.getProducerConfig();
			producerProperties.put(ProducerConfig.KEY_SERIALIZER_CLASS_CONFIG, StringSerializer.class);
			producerProperties.put(ProducerConfig.VALUE_SERIALIZER_CLASS_CONFIG, IntegerSerializer.class);
			IntegrationTestUtils.produceKeyValuesSynchronously(topicList.get(0),
					TwitterTestUtils.generateTweetSentimentsForProducer(), producerProperties);

			ReadOnlyWindowStore<String, AvgValue> store = IntegrationTestUtils.waitUntilStoreIsQueryable("avg-store",
					QueryableStoreTypes.<String, AvgValue>windowStore(), streams);

			// Value expected
			Map<String, AvgValue> expected = new HashMap<String, AvgValue>();
			expected.put("#test1", TwitterTestUtils.meanOfSentiments());
			IntegrationTestUtils.assertThatOldestWindowContains(store, expected);

		} finally {
			if (streams != null) {
				streams.close();
			}
		}
	}

}
