package com.kafka.twitter.streaming.twitter;

import java.util.ArrayList;
import java.util.Collection;

import org.apache.kafka.streams.KeyValue;

import com.kafka.twitter.streaming.TwitterStreaming;
import com.kafka.twitter.streaming.model.agg.AvgValue;

public class TwitterTestUtils {

	public static Collection<KeyValue<String, String>> generateHashtagsForProducer() {
		Collection<KeyValue<String, String>> result = new ArrayList<KeyValue<String, String>>();
		KeyValue<String, String> tweet1 = new KeyValue<String, String>("#test1", TweetTestUtil.tweet1);
		result.add(tweet1);
		return result;
	}
	
	public static Collection<KeyValue<String, Integer>> generateTweetSentimentsForProducer() {
		@SuppressWarnings("serial")
		Collection<KeyValue<String, Integer>> result = new ArrayList<KeyValue<String, Integer>>(){
			{
				add(new KeyValue<String, Integer>("#test1", 5));
				add(new KeyValue<String, Integer>("#test1", 1));
				add(new KeyValue<String, Integer>("#test1", 10));
			}
		};
		return result;
	}
	
	public static double emeaOfSentiments() {
		Double result = null;
		Collection<KeyValue<String, Integer>> vals = generateTweetSentimentsForProducer();
		for (KeyValue<String, Integer> val:vals) {
			if (result == null) result = (double)val.value;
			result = calculateEMA(result, val.value);
			result = Math.round(result * 100.0) / 100.0;
		}
		return result;
	}
	
	/**
	 * Calculate emea
	 * @param emea
	 * @param value
	 * @return
	 */
	protected static Double calculateEMA (double emea, int value){
		return TwitterStreaming.EMA_VAL * value + (1 - TwitterStreaming.EMA_VAL) * emea;
	}
	
	public static AvgValue meanOfSentiments() {
		Integer count = (int)generateTweetSentimentsForProducer().stream().mapToInt(v->v.value).summaryStatistics().getCount();
		Integer sum = (int)generateTweetSentimentsForProducer().stream().mapToInt(v->v.value).summaryStatistics().getSum();
		return new AvgValue(count, sum) ;
	}

	
}
