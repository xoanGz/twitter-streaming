package com.kafka.twitter;

import org.apache.kafka.streams.KafkaStreams;
import org.apache.kafka.streams.state.HostInfo;
import org.apache.log4j.Logger;

import com.kafka.twitter.streaming.TwitterStreaming;
import com.kafka.twitter.streaming.TwitterStreamingImpl;
import com.kafka.twitter.streaming.interactivequeries.SentimentInterativeQueriesRestService;
import com.kafka.twitter.streaming.util.SystemUtil;

/**
 * Create a flow streaming of the topic twitter into twitter-sentiment with a
 * rest API
 */
public class Run {

	final static Logger logger = Logger.getLogger(Run.class);
	
	public static void main(String[] args) throws Exception {
		try {
			logger.info("Entering the Kafka Twitter Streaming \n Creating Stream");
			TwitterStreaming twitterStreaming = new TwitterStreamingImpl();
			KafkaStreams stream = twitterStreaming.run();

			logger.info("Creating endpoint");
			final SentimentInterativeQueriesRestService restService = startRestProxy(stream);

			// Add shutdown hook to respond to SIGTERM and gracefully close
			// Kafka
			Runtime.getRuntime().addShutdownHook(new Thread(() -> {
				try {
					stream.close();
					restService.stop();
				} catch (Exception e) {
					// ignored
				}
			}));


		} catch (Exception e) {
			logger.error("Exiting the Kafka Twitter Streaming with error", e);
		}
	}

	/**
	 * Start the server rest proxy
	 * 
	 * @param streams
	 * @param hostInfo
	 * @return
	 * @throws Exception
	 */
	static SentimentInterativeQueriesRestService startRestProxy(final KafkaStreams streams) throws Exception {
		final String hostname = SystemUtil.getEnv("host.name", "localhost");
		int port = Integer.valueOf(SystemUtil.getEnv("host.port", "3040"));
		final HostInfo restEndpoint = new HostInfo(hostname, port);
		final SentimentInterativeQueriesRestService restService = new SentimentInterativeQueriesRestService(streams,
				restEndpoint);
		restService.start();
		logger.info("Kafka Twitter Streaming listening in " + hostname + ":" + port);
		return restService;
	}
}
