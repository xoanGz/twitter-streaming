package com.kafka.twitter.classify;

import com.kafka.twitter.tweetClassifier.ClassifierGrpc;
import com.kafka.twitter.tweetClassifier.TweetClassifier.Tweet;
import com.kafka.twitter.tweetClassifier.TweetClassifier.ClassificationReply;

import io.grpc.ManagedChannel;
import io.grpc.ManagedChannelBuilder;

public class ClassifyTweetClient {
	
	private final ClassifierGrpc.ClassifierBlockingStub blockingStub;
	
	public ClassifyTweetClient(String domain, int port) {
		ManagedChannel channel = ManagedChannelBuilder.forAddress(domain, port).usePlaintext(true).build();
		blockingStub = ClassifierGrpc.newBlockingStub(channel);
	}
	
	/**
	 * Classify the tweet
	 * Send an grpc request
	 * 
	 * @param id
	 * @param msg
	 * @return
	 */
	public int classifyTweet(String id, String msg) {
		Tweet request = Tweet.newBuilder().setId(id).setBody(msg).build();
		ClassificationReply response = blockingStub.classifyTweet(request);
		return response.getClassPrediction();
	}
}
