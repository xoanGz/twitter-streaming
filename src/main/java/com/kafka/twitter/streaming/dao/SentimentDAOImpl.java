package com.kafka.twitter.streaming.dao;

import org.apache.log4j.Logger;

import com.kafka.twitter.classify.ClassifyTweetClient;
import com.kafka.twitter.streaming.model.tweet.TweetVO;
import com.kafka.twitter.streaming.util.SystemUtil;

public class SentimentDAOImpl implements SentimentDAO {

	final static Logger logger = Logger.getLogger(SentimentDAOImpl.class);
	private static ClassifyTweetClient cli;
	
	public SentimentDAOImpl () {
		if (cli == null) {
			String localhost = SystemUtil.getEnv("classify.tweet.domain", "localhost");
			int port = Integer.parseInt(SystemUtil.getEnv("classify.tweet.port", "50051"));
			cli = new ClassifyTweetClient(localhost, port);
		}
	}
	
	@Override
	public Integer getSentiment(String hashtag, TweetVO tweet) {
		Integer value = 5;
		try {
			logger.debug("gRPC request to classify-tweet");
			value = cli.classifyTweet(hashtag, tweet.getText());
			logger.debug("gRPC response to classify-tweet: " + value);
		} catch (Exception e) {
			String str = "Error getting the sentiments";
			logger.error(str,e);
		}
		return value;
	}

}
