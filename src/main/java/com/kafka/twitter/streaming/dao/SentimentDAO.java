package com.kafka.twitter.streaming.dao;

import com.kafka.twitter.streaming.model.tweet.TweetVO;

public interface SentimentDAO {

	public Integer getSentiment(String hashtag, TweetVO tweet);
	
}
