package com.kafka.twitter.streaming;

import java.io.IOException;

import org.apache.kafka.common.serialization.Serde;
import org.apache.kafka.common.serialization.Serdes;
import org.apache.kafka.common.serialization.StringDeserializer;
import org.apache.kafka.common.serialization.StringSerializer;
import org.apache.kafka.streams.KafkaStreams;
import org.apache.kafka.streams.KeyValue;
import org.apache.kafka.streams.StreamsConfig;
import org.apache.kafka.streams.kstream.Aggregator;
import org.apache.kafka.streams.kstream.Initializer;
import org.apache.kafka.streams.kstream.KStream;
import org.apache.kafka.streams.kstream.KStreamBuilder;
import org.apache.kafka.streams.kstream.TimeWindows;
import org.apache.kafka.streams.kstream.Windowed;
import org.apache.kafka.streams.kstream.internals.WindowedDeserializer;
import org.apache.kafka.streams.kstream.internals.WindowedSerializer;
import org.apache.log4j.Logger;

import com.kafka.twitter.streaming.dao.SentimentDAO;
import com.kafka.twitter.streaming.dao.SentimentDAOImpl;
import com.kafka.twitter.streaming.model.HashTagSentiment;
import com.kafka.twitter.streaming.model.agg.AvgValue;
import com.kafka.twitter.streaming.model.tweet.TweetVO;
import com.kafka.twitter.streaming.util.SerdesUtil;
import com.kafka.twitter.streaming.util.SystemUtil;

/**
 * 
 * This Class will create the stream for Twitter Sentiments
 * 
 */
public class TwitterStreamingImpl implements TwitterStreaming {

	final static Logger logger = Logger.getLogger(TwitterStreamingImpl.class);

	/**
	 * Create and run the Kafka Client for consume tweets topics
	 * 
	 * @param args
	 * @throws IOException
	 */
	public KafkaStreams run() throws Exception {

		/**
		 * Provide the high-level Kafka Streams DSL to specify a Kafka Streams
		 * topology.
		 */
		final KStreamBuilder kStreamBuilder = new KStreamBuilder();

		// 1._ Access to twitter topic
		final KStream<String, TweetVO> tweetsStream = this.readFromTwitterTopic(kStreamBuilder, TOPIC_FROM);

		// 2._ Create stream with sentiment info
		final KStream<String, Integer> sentimentStream = generateTweetSentimentStream(tweetsStream,
				new SentimentDAOImpl());

		// 3. Create the ema sentiment stream
		this.createAggEMAStream(sentimentStream, TWITTER_SENTIMENT_STORE);

		// 4. Create daily average
		Long timeWindowLong = new Long (SystemUtil.getEnv("time-window-ms", "10000"));
		TimeWindows dailyWindows = TimeWindows.of(timeWindowLong).until(timeWindowLong);
		this.createDailyAvg(sentimentStream, dailyWindows, TWITTER_DAILY_AVG_STORE, TOPIC_DAILY_AVG);

		// 5. Start the kafka client
		final KafkaStreams kafkaClient = kafkaClientStart(kStreamBuilder);

		return kafkaClient;
	}

	/**
	 * 
	 * Read form the queue TOPIC_FROM "twitter" and create a KStream
	 * 
	 * @param topicFrom
	 * @return
	 */
	public KStream<String, TweetVO> readFromTwitterTopic(KStreamBuilder kStreamBuilder, String topicFrom) {
		return kStreamBuilder.stream(stringSerde, SerdesUtil.createSerde(TweetVO.class), topicFrom);
	}

	/**
	 * 
	 * Create a new kstream (k,v)->hashtag, sentiment with the information of
	 * the sentiment of the tweet
	 * 
	 * @param kStream
	 * @param sentimentDAO
	 * @return
	 */
	public KStream<String, Integer> generateTweetSentimentStream(final KStream<String, TweetVO> kStream,
			SentimentDAO sentimentDAO) {
		return kStream.map((k, v) -> new KeyValue<String, Integer>(k.replace("#", "").toLowerCase(),
				sentimentDAO.getSentiment(k, v)));
	}

	/**
	 * 
	 * Create the EMA agg value and store it in a Local store.
	 * 
	 * Create the aggregation First, group all the Streaming by Key, it a must
	 * if you want to create an aggregation Then creates and aggregation of the
	 * value, calculating a exponential moving average This value is store in a
	 * KTable changelog stream for exposing the last results by api rest And
	 * also are send to a new topic twitter-sentiment
	 * 
	 * @return
	 */
	public void createAggEMAStream(final KStream<String, Integer> stream, String twitterSentimentStore) {
		stream.groupByKey(stringSerde, Serdes.Integer()).aggregate(new Initializer<HashTagSentiment>() {
			@Override
			public HashTagSentiment apply() {
				return new HashTagSentiment();
			}
		}, new Aggregator<String, Integer, HashTagSentiment>() {
			@Override
			public HashTagSentiment apply(String aggKey, Integer value, HashTagSentiment aggregate) {
				Double emea = aggregate.getEmea() != null ? aggregate.getEmea() : (double) value;
				Double val = EMA_VAL * value + (1 - EMA_VAL) * emea;
				val = Math.round(val * 100.0) / 100.0;
				aggregate.setEmea(val);
				aggregate.setTs(System.currentTimeMillis());
				return aggregate;
			}

		}, sentimentSerde, twitterSentimentStore);
	}

	/**
	 * 
	 * Consolidate a daily average of the raw sentiment
	 * 
	 * @param stream
	 * @param topicDailyAvg
	 */
	public void createDailyAvg(final KStream<String, Integer> stream, TimeWindows windows, final String storeDailyAvg, final String topicDailyAvg) {
		/** windowed string serde */
		WindowedSerializer<String> windowedSerializer = new WindowedSerializer<>(new StringSerializer());
		WindowedDeserializer<String> windowedDeserializer = new WindowedDeserializer<>(new StringDeserializer());
		Serde<Windowed<String>> windowedSerde = Serdes.serdeFrom(windowedSerializer, windowedDeserializer);

		stream.groupByKey(stringSerde, Serdes.Integer()).aggregate(AvgValue::new, (k, v, avgValue) -> avgValue.add(v),
				windows, SerdesUtil.createSerde(AvgValue.class), storeDailyAvg)
				.<Double>mapValues((v) -> {
					double val = (double) v.getSum() / v.getCount();
					val = Math.round(val * 100.0) / 100.0;
					return val ;
				}).to(windowedSerde, doubleSerde, topicDailyAvg);
	}

	/**
	 * 
	 * Always (and unconditionally) clean local state prior to starting the
	 * processing topology. We opt for this unconditional call here because this
	 * will make it easier for you to play around with the example when
	 * resetting the application for doing a re-run (via the Application Reset
	 * Tool,
	 * http://docs.confluent.io/current/streams/developer-guide.html#application-reset-tool).
	 * The drawback of cleaning up local state prior is that your app must
	 * rebuilt its local state from scratch, which will take time and will
	 * require reading all the state-relevant data from the Kafka cluster over
	 * the network. Thus in a production scenario you typically do not want to
	 * clean up always as we do here but rather only when it is truly needed,
	 * i.e., only under certain conditions (e.g., the presence of a command line
	 * flag for your app). See `ApplicationResetExample.java` for a
	 * production-like example.
	 * 
	 * Now that we have finished the definition of the processing topology we
	 * can actually run it via `start()`. The Streams application as a whole can
	 * be launched just like any normal Java application that has a `main()`
	 * method.
	 * 
	 * @param streamsConfig
	 * @param kStreamBuilder
	 * @return
	 */
	private KafkaStreams kafkaClientStart(final KStreamBuilder kStreamBuilder) {
		final StreamsConfig streamsConfig = new StreamsConfig(SystemUtil.getProperties());
		KafkaStreams kafkaClient = new KafkaStreams(kStreamBuilder, streamsConfig);
		kafkaClient.cleanUp();
		kafkaClient.start();
		return kafkaClient;
	}

}
