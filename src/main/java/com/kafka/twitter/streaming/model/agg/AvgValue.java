package com.kafka.twitter.streaming.model.agg;

/**
 * 
 * Class for calculate the mean of sentiment for a tweet
 * 
 * 
 * @author xoan
 *
 */
public class AvgValue {
    private Integer count;
    private Integer sum;

    /**
     * konstructor
     */
    public AvgValue() {
        this.count = 0;
        this.sum = 0;
    }
    
    public AvgValue(Integer count, Integer sum) {
		super();
		this.count = count;
		this.sum = sum;
	}

	/**
     * Add value to calculate mean
     * @param num
     * @return
     */
    public AvgValue add(Integer num) {
    	this.count++;
    	this.sum += num;
    	return this;
    }

	public Integer getCount() {
		return count;
	}

	public void setCount(Integer count) {
		this.count = count;
	}

	public Integer getSum() {
		return sum;
	}

	public void setSum(Integer sum) {
		this.sum = sum;
	}
	
	@Override
	public boolean equals(Object obj) {
	    if (obj == null) {
	        return false;
	    }
	    if (!AvgValue.class.isAssignableFrom(obj.getClass())) {
	        return false;
	    }
	    final AvgValue other = (AvgValue) obj;
	    if (this.count != other.count) {
	        return false;
	    }
	    if (this.sum != other.sum) {
	        return false;
	    }
	    return true;
	}

	@Override
	public int hashCode() {
	    int hash = 3;
	    hash = 53 * hash + this.count;
	    hash = 53 * hash + this.sum;
	    return hash;
	}
    
}
