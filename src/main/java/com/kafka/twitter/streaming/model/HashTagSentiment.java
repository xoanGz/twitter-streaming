package com.kafka.twitter.streaming.model;

/**
 * 
 * Sentiment of a hashtag
 * 
 */
public class HashTagSentiment {
	
	private Double emea;
	private long ts;

	public HashTagSentiment() {
	}

	public Double getEmea() {
		return emea;
	}

	public void setEmea(Double emea) {
		this.emea = emea;
	}

	public long getTs() {
		return ts;
	}

	public void setTs(long ts) {
		this.ts = ts;
	}

	@Override
	public String toString() {
		return "HashTagSentiment [emea=" + emea + ", ts=" + ts + "]";
	}
	
}