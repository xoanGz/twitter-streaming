package com.kafka.twitter.streaming.util;

import java.util.Properties;

import org.apache.kafka.common.serialization.Serdes;
import org.apache.kafka.streams.StreamsConfig;
import org.apache.kafka.streams.processor.WallclockTimestampExtractor;
import org.apache.log4j.Logger;

public class SystemUtil {
	
	final static Logger logger = Logger.getLogger(SystemUtil.class);
	private static boolean debug = Boolean.TRUE;
	private static final String APP_ID = "app.id";
	public static final String BOOTSTRAP_SERVERS_CONFIG = "bootstrap.servers";
	public static final String STATE_DIR_CONFIG = "state.dir";
	
	public static Properties getProperties() {
		Properties settings = new Properties();
		// Set a few key parameters
		settings.put(StreamsConfig.APPLICATION_ID_CONFIG, getEnv(APP_ID, "twitter-streaming"));
		// Kafka bootstrap server (broker to talk to);  
		settings.put(StreamsConfig.BOOTSTRAP_SERVERS_CONFIG, getEnv(BOOTSTRAP_SERVERS_CONFIG,"localhost:9092"));
		// default serdes for serialzing and deserializing key and value from and to streams in case no specific Serde is specified
		settings.put(StreamsConfig.KEY_SERDE_CLASS_CONFIG, Serdes.String().getClass().getName());
		settings.put(StreamsConfig.VALUE_SERDE_CLASS_CONFIG, Serdes.String().getClass().getName());
		settings.put(StreamsConfig.STATE_DIR_CONFIG, getEnv(STATE_DIR_CONFIG, "temp"));
		// to work around exception Exception in thread "StreamThread-1" java.lang.IllegalArgumentException: Invalid timestamp -1
		// at org.apache.kafka.clients.producer.ProducerRecord.<init>(ProducerRecord.java:60)
		// see: https://groups.google.com/forum/#!topic/confluent-platform/5oT0GRztPBo
		settings.put(StreamsConfig.TIMESTAMP_EXTRACTOR_CLASS_CONFIG, WallclockTimestampExtractor.class);
		return settings;
	}
	

	/**
	 * Get value form system environment
	 * @param key
	 * @return
	 */
	public static String getEnv(String key) {
		return getEnv(key, null);
	}
	
	/**
	 * Get value form system environment
	 * with default value
	 * 
	 * @param key
	 * @return
	 */
	public static String getEnv(String key, String def) {
		String result = null;
		if (debug) {
			logger.info("Getting env var:" + key);
		}
		result = System.getenv(key);
		if (result==null) result = def;
		
		if (debug) {
			logger.info("Env var:" + key + " has value " + result);
		}
		return result;
	}
	
}
