package com.kafka.twitter.streaming.util;

import java.util.HashMap;
import java.util.Map;

import org.apache.kafka.common.serialization.Deserializer;
import org.apache.kafka.common.serialization.Serde;
import org.apache.kafka.common.serialization.Serdes;
import org.apache.kafka.common.serialization.Serializer;
import org.apache.log4j.Logger;

import com.kafka.twitter.streaming.model.serdes.JsonPOJODeserializer;
import com.kafka.twitter.streaming.model.serdes.JsonPOJOSerializer;

public class SerdesUtil {
	
	final static Logger logger = Logger.getLogger(SerdesUtil.class);


	/**
	 * Create serde from an object
	 * @param type
	 * @return
	 */
	public static <T> Serde<T> createSerde(Class<T> type) {
		// TODO: the following can be removed with a serialization factory
		Map<String, Object> serdeProps = new HashMap<>();

		final Serializer<T> serializer = new JsonPOJOSerializer<>();
		serdeProps.put("JsonPOJOClass", type);
		serializer.configure(serdeProps, false);

		final Deserializer<T> deserializer = new JsonPOJODeserializer<>();
		serdeProps.put("JsonPOJOClass", type);
		deserializer.configure(serdeProps, false);

		return Serdes.serdeFrom(serializer, deserializer);
	}
	
	
}
