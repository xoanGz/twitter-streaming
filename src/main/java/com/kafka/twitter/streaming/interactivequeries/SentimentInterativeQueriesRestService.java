package com.kafka.twitter.streaming.interactivequeries;

import java.util.EnumSet;
import java.util.List;

import javax.servlet.DispatcherType;
import javax.ws.rs.GET;
import javax.ws.rs.NotFoundException;
import javax.ws.rs.Path;
import javax.ws.rs.PathParam;
import javax.ws.rs.Produces;
import javax.ws.rs.client.Client;
import javax.ws.rs.client.ClientBuilder;
import javax.ws.rs.core.GenericType;
import javax.ws.rs.core.MediaType;

import org.apache.kafka.streams.KafkaStreams;
import org.apache.kafka.streams.state.HostInfo;
import org.apache.kafka.streams.state.QueryableStoreTypes;
import org.apache.kafka.streams.state.ReadOnlyKeyValueStore;
import org.eclipse.jetty.server.Server;
import org.eclipse.jetty.servlet.FilterHolder;
import org.eclipse.jetty.servlet.ServletContextHandler;
import org.eclipse.jetty.servlet.ServletHolder;
import org.eclipse.jetty.servlets.CrossOriginFilter;
import org.glassfish.jersey.jackson.JacksonFeature;
import org.glassfish.jersey.server.ResourceConfig;
import org.glassfish.jersey.servlet.ServletContainer;

import com.kafka.twitter.streaming.TwitterStreaming;
import com.kafka.twitter.streaming.model.HashTagSentiment;

@Path("twitter")
public class SentimentInterativeQueriesRestService {

	private final KafkaStreams streams;
	private final MetadataService metadataService;
	private final HostInfo hostInfo;
	private final Client client = ClientBuilder.newBuilder().register(JacksonFeature.class).build();
	private Server jettyServer;

	public SentimentInterativeQueriesRestService(KafkaStreams streams, final HostInfo hostInfo) {
		this.streams = streams;
		this.metadataService = new MetadataService(streams);
		this.hostInfo = hostInfo;
	}
	/**
	 * Get a key-value pair from a KeyValue Store
	 *
	 * @param hashtag
	 *            the key to get
	 * @return {@link KeyValueBean} representing the key-value pair
	 */
	@GET
	@Path("/sentiment/{hashtag}")
	@Produces(MediaType.APPLICATION_JSON)
	public KeyValueBean getSentiment(@PathParam("hashtag") final String hashtag) {

		//TODO:
		// The hashtag might be hosted on another instance. We need to find
		// which instance it is on
		// and then perform a remote lookup if necessary.
//
//		final HostStoreInfo host = metadataService.streamsMetadataForStoreAndKey(
//				TwitterStreaming.TWITTER_SENTIMENT_STORE, hashtag, new StringSerializer());
//
//		// hashtag is on another instance. call the other instance to fetch the
//		// data.
//		
//		if (!thisHost(host)) {
//			return fetchSentiment(host, "twitter/sentiment/" + hashtag);
//		}

		return getSentiment(hashtag, TwitterStreaming.TWITTER_SENTIMENT_STORE);
	}

	/**
	 * Get the sentiment for the local store database
	 * 
	 * @param hashtag
	 * @param storeName
	 * @return
	 */
	public KeyValueBean getSentiment(final String hashtag, String storeName) {

		// Lookup the KeyValueStore with the provided storeName
		final ReadOnlyKeyValueStore<String, HashTagSentiment> store = streams.store(storeName,
				QueryableStoreTypes.<String, HashTagSentiment>keyValueStore());
		if (store == null) {
			throw new NotFoundException();
		}

		// Get the value from the store
		final HashTagSentiment value = store.get(hashtag.replace("#", "").toLowerCase());
		if (value == null) {
			throw new NotFoundException();
		}
		return new KeyValueBean(hashtag, value);
	}

	private KeyValueBean fetchSentiment(final HostStoreInfo host, final String path) {
		return client.target(String.format("http://%s:%d/%s", host.getHost(), host.getPort(), path))
				.request(MediaType.APPLICATION_JSON_TYPE).get(new GenericType<KeyValueBean>() {
				});
	}

	private boolean thisHost(final HostStoreInfo host) {
		return host.getHost().equals(hostInfo.host()) && host.getPort() == hostInfo.port();
	}

	  /**
	   * Get the metadata for all of the instances of this Kafka Streams application
	   * @return List of {@link HostStoreInfo}
	   */
	  @GET()
	  @Path("/instances")
	  @Produces(MediaType.APPLICATION_JSON)
	  public List<HostStoreInfo> streamsMetadata() {
	    return metadataService.streamsMetadata();
	  }

	  /**
	   * Get the metadata for all instances of this Kafka Streams application that currently
	   * has the provided store.
	   * @param store   The store to locate
	   * @return  List of {@link HostStoreInfo}
	   */
	  @GET()
	  @Path("/instances/{storeName}")
	  @Produces(MediaType.APPLICATION_JSON)
	  public List<HostStoreInfo> streamsMetadataForStore(@PathParam("storeName") String store) {
	    return metadataService.streamsMetadataForStore(store);
	  }
	
	/**
	 * Start an embedded Jetty Server
	 * 
	 * @throws Exception
	 */
	public void start() throws Exception {
		ServletContextHandler context = new ServletContextHandler(ServletContextHandler.SESSIONS);
		context.setContextPath("/");

		/**
		 * CORS
		 */
		 FilterHolder filterHolder = new FilterHolder(new CrossOriginFilter());
		 filterHolder.setName("cross-origin");
		 filterHolder.setInitParameter("allowedOrigins", "*");
		 filterHolder.setInitParameter("allowedMethods", "GET, POST");
		 context.addFilter(filterHolder, "/*", EnumSet.of(DispatcherType.REQUEST));
		
		jettyServer = new Server(hostInfo.port());
		jettyServer.setHandler(context);

		ResourceConfig rc = new ResourceConfig();
		rc.register(this);
		rc.register(JacksonFeature.class);

		ServletContainer sc = new ServletContainer(rc);
		ServletHolder holder = new ServletHolder(sc);
		context.addServlet(holder, "/*");

		jettyServer.start();
	}

	/**
	 * Stop the Jetty Server
	 * 
	 * @throws Exception
	 */
	public void stop() throws Exception {
		if (jettyServer != null) {
			jettyServer.stop();
		}
	}
}
