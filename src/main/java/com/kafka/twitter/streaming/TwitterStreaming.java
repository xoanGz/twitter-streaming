package com.kafka.twitter.streaming;

import org.apache.kafka.common.serialization.Serde;
import org.apache.kafka.common.serialization.Serdes;
import org.apache.kafka.streams.KafkaStreams;

import com.kafka.twitter.streaming.model.HashTagSentiment;
import com.kafka.twitter.streaming.util.SerdesUtil;

/**
 * 
 * This Class will create the stream for Twitter Sentiments
 * 
 */
public interface TwitterStreaming {

	/** Topics and KStore configurations */
	public static final String TOPIC_FROM = "twitter";
	public static final String TOPIC_DAILY_AVG = "twitter-daily-avg";
	public static final String TWITTER_SENTIMENT_STORE = "twitter-sentiment-store";
	public static final String TWITTER_DAILY_AVG_STORE = "twitter-daily-avg-store";
	/** Aggregation and serdes configurations */
	public static final Float EMA_VAL = 0.6f;
	public static final Serde<HashTagSentiment> sentimentSerde = SerdesUtil.createSerde(HashTagSentiment.class);
	public static final Serde<String> stringSerde = Serdes.String();
	public static final Serde<Double> doubleSerde = Serdes.Double();

	public KafkaStreams run() throws Exception;

}
