FROM java:8-jre 
MAINTAINER dataspartan 
ADD ./target/streaming-0.0.1-SNAPSHOT-jar-with-dependencies.jar / 
CMD ["sh", "-c", "java -jar streaming-0.0.1-SNAPSHOT-jar-with-dependencies.jar"]
